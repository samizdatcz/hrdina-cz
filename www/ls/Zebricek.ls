lineHeight = 40
itemCount = 20
svgWidth = 61
class ig.Zebricek
  (@parentElement, data) ->
    @graphTip = new ig.GraphTip @parentElement
    @processData data
    @recount!
    # @logStats!
    @parentElement.append \div
      ..attr \class \titles
      ..append \h1 .html "největší hrdina"
      ..append \h1 .html "nejzápornější postava"
    @element = @parentElement.append \div
      ..attr \class \zebricek
    @svg = @parentElement.append \svg
      ..attr \width svgWidth
      ..attr \height lineHeight * itemCount
    @lists = @element.selectAll \ul .data @heroesAndVillains .enter!append \ul
      ..attr \class (d, i) -> if i then "villains" else "heroes"
    @redraw -> it.index isnt null #and it.index < 20 #it.filteredVoters.length >= 10

  redraw: (filterFn) ->
    @lists.selectAll \li .data (-> it.filter filterFn)
      ..enter!append \li
        ..append \span
          ..attr \class \name
          ..html (.name)
        ..append \span
          ..attr \class \count
        ..append \div
          ..attr \class \bar
        ..on \mouseover ~> @highlightPath it.name
      ..select \.count
        ..html -> "#{it.count} hlas#{if it.count > 4 then 'ů' else 'y'}"
      ..select \.bar
        ..selectAll \div .data (-> [0 to it.filteredVoters.length / 30])
          ..enter!append \div
          ..exit!remove!
      ..style \top -> "#{it.index * lineHeight}px"
    connections = @heroes
      .filter filterFn
      .filter -> it.opposing && filterFn it.opposing
      .map ->
        name: it.name
        path: [it.index, it.index, it.opposing.index, it.opposing.index]

    path = d3.svg.line!
      ..x (d, i) -> switch i
        | 0 => 10
        | 1 => 15
        | 2 => svgWidth - 15
        | 3 => svgWidth - 10
      ..y -> it * lineHeight + 10
      ..interpolate \monotone
    @paths = @svg.selectAll \path .data(connections, (.name))
      ..enter!append \path
        ..attr \d -> path it.path
    # @showPopup @heroes.0, 0, 0

  highlightPath: (name) ->
    @paths.classed \active -> it.name is name

  showPopup: (d, i, ii) ->
    x = if ii then 750 else 250
    @graphTip.display x, 50 + i * lineHeight, getContent d

  recount: (filterFn) ->
    for list in [@heroes, @villains]
      list.forEach (.recount filterFn)
      list.sort (a, b) -> b.count - a.count
      for item, index in list
        item.index = if index < itemCount then index else null
        item.percent = item.count / list[0].count

  processData: (data) ->
    @heroes = []
    @villains = []
    @heroesAndVillains = [@heroes, @villains]
    heroesAssoc = {}
    villainsAssoc = {}
    for datum in data
      for field,value of datum
        globals[field] ?= {}
        globals[field][value] ?= 0
        globals[field][value]++
    for datum in data
      if datum.hrdina != null
        if heroesAssoc[datum.hrdina] is void
          @heroes.push heroesAssoc[datum.hrdina] = new Person datum.hrdina
        heroesAssoc[datum.hrdina].addVoter datum
      if datum.zaporak != null
        if villainsAssoc[datum.zaporak] is void
          @villains.push villainsAssoc[datum.zaporak] = new Person datum.zaporak
        villainsAssoc[datum.zaporak].addVoter datum

    for hero in @heroes
      if villainsAssoc[hero.name]
        hero.opposing = villainsAssoc[hero.name]
        villainsAssoc[hero.name].opposing = hero
    for villain in @villains
      if heroesAssoc[villain.name]
        villain.opposing = heroesAssoc[villain.name]
        heroesAssoc[villain.name].opposing = villain

  logStats: ->
    fields =
      "vek": [0 to 2]
      "kraj": [1 to 14]
      "vzdelani": [0 to 2]
      "risk": [0 to 1]
      "vlast": [0 to 2]
      "zivot1": [0 to 6]
      "zivot2": [0 to 6]
      "obec": [0 to 4]
      "cesi": [0 to 2]
    tops = ((@heroes.slice 0, 10) ++ (@villains.slice 0, 10))
    # tops.length = 2
    lines = for person in tops
      out = [person.name]
      for field, options of fields
        values = options.map (option) ->
          person.locals[field][option]
        max = d3.max values
        out.push ...values.map -> ((it || 0) / 1).toString!replace '.' ','
      out.join "\t"
    header = ["name"]
    for field, options of fields
      for option in options
        header.push "#field-#option"
    lines.unshift header.join "\t"
    console.log lines.join "\n"


getContent = (person) ->
  # console.log person
  d = person.locals
  veky = [
    * vek: "<35", percent: d.vek."0" || 0
    * vek: "54", percent: d.vek."1" || 0
    * vek: ">55", percent: d.vek."2" || 0
  ]

  base = d3.select document.createElement \div
    ..append \div
      ..attr \class \
  base.html!

globals = {}
class Person
  (@name) ->
    @voters = []
    @count = 0
    @locals = {}

  addVoter: (datum) ->
    @voters.push datum
    for field, value of datum
      @locals[field] ?= {}
      @locals[field][value] ?= 0
      @locals[field][value] += 1 / globals[field][value]

  recount: (filterFn) ->
    @filteredVoters =
      | filterFn  => @voters.filter filterFn
      | otherwise => @voters
    @count = @filteredVoters.length
