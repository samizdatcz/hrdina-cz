---
title: "Hrdina.cz - analýza průběžných výsledků ankety"
author: "petr.koci@samizdat.cz 724 339 831 / marcel.sulek@samizdat.cz"
date: "27. května 2015"
output: html_document
---

## Co by mohlo být zajímavé dál rozpracovat
- vzít méně frekventovaná jména ze seznamu a připomenout jejich příběhy (Jan Smudek, Přemysl Pitter, Josef František, Mořic Levy...)
- zaměřit se na [postavy, které se objevují zároveň mezi kladnými i zápornými hrdiny](#padouch)
- rozdíly muži/ženy, staří/mladí, město/vesnice
- vzít to obráceně: "kdo hlasoval pro Gottwalda x kdo hlasoval pro Havla" 

## Poznámky ke kvalitě a úplnosti dat
- data reflektují průběžný stav hlasování ke 14. květnu 2015
- počet hlasujících (490) je poměrně nízký
- výběr vzorku z populace není náhodný ani reprezentativní, proto nelze závěry zobecňovat; vždy je nutné uvést, že se jedná pouze o názory účastníků ankety
- vzhledem k nízkému počtu hlasů a chybějící kontrole identity hlasujících je poměrně snadné výsledky ankety zmanipulovat
- přímé srovnání s mezinárodními výsledky obdobného [projektu EBU](https://samizdat.cz/data/hrdina-cz/data/ebu.pdf) není vzhledem k odlišné metodě sběru dat možné
- formulář ankety je nedostatečně zabezpečený proti zadávání nekonzistentních dat: je (nebo bylo) možné zadávat nepřiměřeně dlouhý text v otázce se jménem hradiny/antihrdiny, je (nebo bylo) možné vybírat více než tři i méně než tři obrázky hrdinství... vyhodnocení ankety to značně komplikuje
- data v pých "hrdina" a "zaporak"" jsme ručně vyčistili, ověřili a sjednotili způsob zápisu jmen (KLAUS V. na Václav Klaus, a podobně)
- pokud byla uvedena více než jedna postava, počítali jsme pouze první uvedené jméno (Gabčík a Kubiš = Jozef Gabčík)
- pokud nebylo možné postavu na základě uvedených údajů jednoznačně identifikovat (např. "moje máma", "Českoslovenští letci", "parašutisté", "bratři Mašínové", "neznámý vojín", "obyčejný člověk"), do výsledků jsme ji nezapočítali
- uznáváme i fiktivní postavy ("Jára Cimrman", "král Kazisvět", "Josef Švejk")
- bylo-li uvedeno jen příjmení, započítali jsme jeho nejznámějšího nositele ("Masaryk" = "Tomáš Garrigue Masaryk", "Babinský" = "Václav Babinský")
- neuznali jsme zvířata ("Šemík")

## Základní údaje o hlasujících
Prvotní [analýzu s grafy](https://samizdat.cz/data/hrdina-cz/data/prubezne-vysledky.pdf) již zpracoval Petr Zettner.

## Vizualizace průběžných výsledků
```{r, echo=FALSE, results='asis'}
cat('<iframe src="https://samizdat.cz/data/hrdina-cz/www" class="ig" width="1000" height="2350" scrolling="no" style="margin: 10px 0px;" frameborder="0"></iframe>')
```

Grafiku můžete vkládat do stránek pomocí následujícího kódu:
```html
<iframe src="https://samizdat.cz/data/hrdina-cz/www" class="ig" width="1000" height="2350" scrolling="no" style="margin: 10px 0px;" frameborder="0"></iframe>
```

Pokud byste chtěli upravit velikost grafu, omezit zobrazení výsledků například jen na top 20, napište prosím svůj požadavek na marcel.sulek@samizdat.cz.

## <a name="padouch"></a>Padouch, nebo hrdina, všechno jedna rodina
Postavy, které se objevují zároveň mezi kladnými i zápornými hrdiny. Kliknutím na názvy sloupců můžete změnit řazení tabulky.

```{r, echo=FALSE, results='asis'}
cat('<iframe src="//d1umsnw3kzdhxo.cloudfront.net/lBeF0/3/" frameborder="0" allowtransparency="true" allowfullscreen="allowfullscreen" webkitallowfullscreen="webkitallowfullscreen" mozallowfullscreen="mozallowfullscreen" oallowfullscreen="oallowfullscreen" msallowfullscreen="msallowfullscreen" width="700" height="550"></iframe>')
```

## Rozdělení hlasů podle věku, vdělání a bydliště respondentů

- regionální rozdíly jsou vzhledem k nízkému počtu hlasujících zanedbatelné
- je to velmi malý vzorek, rovnováhu může vychýlit i několik málo hlasů – výsledky proto nelze zobecňovat, můžeme jen říci, že takto hlasovali dosavadní účastníci ankety, kterých je cca 500
- starší lidé (nad 55 let) hlasují o něco častěji pro Jana Husa a Miladu Horákovou, mladší (do 35) pro TGM, Václava Havla a parašutisty, kteří provedli atentát na Heydricha
- pro starší lidi jsou největší padouši o něco častěji Václav Havel, Václav Klaus, Vasil Biľak, Gustáv Husák a Miroslav Kalousek, pro mladší Emanuel Moravec, Karel Čurda, Edvard Beneš a Miloš Zeman
- lidé s vysokoškolským vzděláním hlasují o něco častěji pro Jozefa Gabčíka a svatého Václava, lidé se základním vzděláním pro Jana Žižku a Karla IV.
- pro vysokoškoláky je větším padouchem Klement Gottwald a Emanuel Moravec, pro účastníky ankety se základním vzděláním Miloš Zeman, Václav Havel, Václav Klaus a Miroslav Kalousek
- TGM, Kubiš a Palach jsou častěji hrdinové pro lidi, kteří uvedli, že by byli ochotni bránit svou vlast se zbraní v ruce, pro ty, kteří uvedli, že by do boje za vlast nešli, je častěji hrdinou Václav Havel a Jozef Gabčík
- pro ty, kdo by šli bránit vlast, je padouchem častěji Vasil Biľak, Václav Havel, Emanuel Moravec a Karel Čurda; pro ty, kdo by vlast se zbraní nebránili, Václav Klaus, Miroslav Kalousek a Gustáv Husák
- pro obyvatele menších obcí je o něco častěji než pro lidi z velkoměst hrdinou Jan Kubiš (u Jozefa Gabčíka je to však obráceně)
- Jan Žižka vítězí u lidí ze středně velkých obcí (20 - 50 tisíc obyvatel), na venkově a ve velkých městech boduje podstatně méně
- svatý Václav má silnou pozici v Praze a ostatních městech na 100 tisíc obyvatel
- Klement Gottwald je padouchem hlavně pro obyvatele středně velkých měst (20 - 50 tisíc obyvatel), v menších obcích a velkých městech už jeho pozice tak jednoznačná není

[Tabulka s rozdělením hlasů](https://samizdat.cz/data/hrdina-cz/data/hrdinove-rozdeleni.xlsx)



