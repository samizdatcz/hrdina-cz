require! fs
lines = fs.readFileSync "#__dirname/../data/clean.tsv" .toString!split "\n"
out = []
allHrdinove = {}
lines.shift!
items = <[id datum pohlavi vek vzdelani kraj obec risk zivot1 zivot2 vlast hrdina zaporak hrdinstvi cesi email Column]>
allValues = {}
for line in lines
  parts = line.split "\t"
  hrdinstvi = parts[13]
  continue unless hrdinstvi
  hrdinove = hrdinstvi.split ","
  for hrdina in hrdinove
    if allHrdinove[hrdina] is void
      allHrdinove[hrdina] = {}
    obj = allHrdinove[hrdina]
    for item, index in items
      continue if item not in <[pohlavi vek vzdelani kraj obec risk zivot1 zivot2 vlast]>
      value = "#{item}-#{parts[index]}"
      allValues[value] = 1
      obj[value] ?= 0
      obj[value]++

values = for value of allValues
  value
values.sort (a, b) -> if a > b then 1 else -1

lines = for hrdina, data of allHrdinove
  out = for value in values
    data[value] || 0
  out.unshift hrdina
  out.join "\t"
values.unshift "hrdina"
lines.unshift values.join "\t"
fs.writeFileSync "#__dirname/../data/obrazky.tsv", lines.join "\n"
